# http-server


## Clone
```
 git clone https://gitlab.com/abhaykoushal/http-server.git
```
## Starting Server
```
 python3 server.py [port] [logging_level{0,1,2}]
```
## Automated Testing
```
- python3 server.py [port] [logging_level{0,1,2}]
- python3 testAll.py [port]
```
